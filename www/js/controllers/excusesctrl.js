angular.module('fuckWork.controllers.excusesctrl', [])

.controller('ExcusesCtrl', ['$scope', 'Excuses', '$timeout', function($scope, Excuses, $timeout) {
    $scope.gotAnExcuse = true;
    $scope.excuseList = Excuses.getExcuses();

    $scope.generateExcuse = function() {
      $scope.gotAnExcuse = false;
      $scope.fetching = "Thinking of a killer excuse to skip work";
      $timeout(function () {
        $scope.excuse = $scope.excuseList[Math.floor(Math.random() * $scope.excuseList.length)];
        $scope.gotAnExcuse = true;
      }, 1200);

    };
}]);
