angular.module('fuckWork.factories.excuses', [])
  .factory('Excuses', [function () {
    var obj = {
      excuses: [
        "Suffered a penis injury last night after meeting up with a tinderlla",
        "I'm sick with the flu",
        "Pulled my hamstring playing pick-up basketball last night. Can't even get out of bed.",
        "My friend got a DUI last night. Have to bail him/her out of jail",
        "My A/C stopped working. Having a repairman come out to the house to fix it.",
        "I have to study for 2 mid-terms tomorrow.",
        "Some family stuff has come up that I need to take care of",
        "Your neighbor called - the water pipes 'broke' and your basement is flooding. Got to get home ASAP!",
        "If your boss is a pet lover, go with a pet emergency. Animal #lovers will let you out of work every time!",
        "You've got a fake food poisoning... works without fail! Just be sure not to blame it on your boss's favorite restaurant.",
        "You have diarrhea - everybody will be so embarrassed to question you about it that you'll be out of the office in 5 minutes!",
        "Your car broke down on the side of the road, and the tow truck's taking forever.",
        "Your cousin went into labor, and you're the only person who lives close enough to get her to the hospital in #time!",
        "You chipped a tooth – what a klutz! – and you now have to make an emergency dentist visit.",
        "If your boss is a guy, just say you're having 'womanly issues'... no guy has the guts to question it!",
        "Lice - yikes! No one wants you coming to work with lice. This might even get you an extra day if you claim to be treating your house, too!",
        "Tell your boss that you had annual blood work, and there was an 'irregularity'... you'll have to go in right away for follow-up testing.",
        "You've been summoned to court. No one can argue with the law!",
        "Claim a urinary track infection. This one's great because it can show up overnight and can clear in a day or two without medication, so no need for a doctor's excuse!",
        "Your best friend's potentially-violent #boyfriend has kicked her out, and you've got to help her move – today!",
        "You locked your keys in your car. The locksmith's on the way, but he said it might be hours before he arrives.",
        "A close family member was in a car #accident. You don't know if its serious or not, but you just couldn't concentrate at work unless you go check on them.",
        "You've caught a stomach virus. This one's great because you can claim whatever hour length you need… 18-hour #stomach virus, 24-hour, 48-hour bug… suits your needs!",
        "Your septic tank overflowed, and your yard is flooded with sewage. No one will question your need to take care of this one!",
        "A tree in your neighborhood fell into the street, blocking the road. The Department of Transportation will respond as soon as they can, but who knows when that will be?",
        "You got stung by a bee last night and had no idea you were allergic… now your #face is so swollen you can't see to drive.",
        "You carpool to work, and your carpool group totally forgot you this morning. Now, you'll have to try to hitch a ride with someone else – surely your #boss won't encourage hitch hiking!",
        "Just say, 'I threw my back out.' People use this excuse all the #time, and no one really knows what it means – but no one seems to question it!",
        "You've got horrible hemorrhoids and your doctor advised you to soak for several hours in a salt water bath. No one will have the audacity to tell you to come on in with this one!",
        "You've got a 'reoccurring gynecological condition'. Get out of work, no questions asked...",
        "A migraine is a great excuse. Anyone who's really had one will know how painful they are, and reoccurring migraines = more days off!",
        "Claim first degree burns in a cooking #accident. Just be sure to say it's on a #body part you can easily cover up for the next few days at work!",
        "You were witness to a hit-and-run #accident, and you've got to go to the police station and give a statement. Again, no one argues with the law!",
        "Just say, 'My spouse/child/roommate is too sick to get out of #bed, so I've got to stay home and care for him/her.'",
        "Someone tried to burglarize your home last night, and you're getting a security system installed today. No one will ask you to jeopardize your personal safety!",
        "Call your boss and say, 'My car was repossessed last night, and I don't have any #money for a taxi!' In this economy, who could blame you?",
        "Tell your #boss that the spring on your garage door snapped, and you're not strong enough to raise the garage door to get your car out. Where did I get this crazy idea? Oh, because it really happened to me! :)"
      ],

      getExcuses: function () {
        return obj.excuses;
      }

    };

    return obj;
  }]);
